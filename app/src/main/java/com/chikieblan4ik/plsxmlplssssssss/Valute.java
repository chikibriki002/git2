package com.chikieblan4ik.plsxmlplssssssss;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.widget.ImageView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Valute {

    private String numCode;
    private String charCode;
    private String nominal;
    private String name;
    private double price;
    private String imageName;

    public Valute(String numCode, String charCode, String nominal, String name, double price) {
        this.numCode = numCode;
        this.charCode = charCode;
        this.nominal = nominal;
        this.name = name;
        this.price = price;
        this.imageName = charCode.substring(0, 2).toLowerCase();
    }

    @Override
    public String toString() {
        return charCode;
    }

    public String getImageName() {
        return imageName;
    }

    public String getNumCode() {
        return numCode;
    }

    public String getCharCode() {
        return charCode;
    }

    public String getNominal() {
        return nominal;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}