package com.chikieblan4ik.plsxmlplssssssss;

import android.util.Log;
import android.util.Xml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Api {

    private String rezult;

    private static final String TAG = "dinamik";


    public HttpURLConnection startConnection(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            return connection;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public String responseFromStream(BufferedReader in) {
        StringBuilder response = new StringBuilder();
        String temp;
        try {
            while ((temp = in.readLine()) != null) {
                response.append(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }


    public String makeLink(String[] keys, String[] values) {
        StringBuilder constructor = new StringBuilder();
        for (int i = 0; i < keys.length; i++) {
            constructor.append(keys[i]);
            constructor.append("=");
            constructor.append(values[i]);
            constructor.append("&");
        }
        return constructor.substring(0, constructor.length() - 1);
    }

    public String get(final String url) {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = startConnection(url);
                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "windows-1251"));
                    rezult = responseFromStream(in);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "Error in GET method: " + e);
                }
            }
        };

        Thread th = new Thread(run);
        try {
            th.start();
            th.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "get: " + e);
        }

        return rezult;
    }

    String post(final String[] keys, final String[] values, final String url) {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = startConnection(url);
                    String link = makeLink(keys, values);

                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);
                    connection.getOutputStream().write(link.getBytes());
                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rezult = responseFromStream(in);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "Error in POST method: " + e);
                }
            }
        };

        Thread th = new Thread(run);
        try {
            th.start();
            th.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rezult;
    }
}
