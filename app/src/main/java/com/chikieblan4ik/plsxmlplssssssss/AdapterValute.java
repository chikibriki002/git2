package com.chikieblan4ik.plsxmlplssssssss;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

public class AdapterValute extends BaseAdapter {

    private static final String TAG = "trikotatrihvosta";
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Valute> valutesArrToday;
    Map<String, Valute> valutesMapYesterday;
    String drawStr = "drawable";

    AdapterValute (Context context, ArrayList<Valute> valuteToday, Map<String, Valute> valuteYesterday) {
        ctx = context;
        this.valutesArrToday = valuteToday;
        this.valutesMapYesterday = valuteYesterday;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return valutesArrToday.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return valutesArrToday.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item, parent, false);
        }

        Valute oneValuteToday = valutesArrToday.get(position);
        double priceToday = oneValuteToday.getPrice();
        double priceYesterday = valutesMapYesterday.get(oneValuteToday.getCharCode()).getPrice();
        if (priceToday > priceYesterday) {
            ((ImageView) view.findViewById(R.id.imgArrow)).setBackgroundResource(R.drawable.arrow_up);
        } else {
            ((ImageView) view.findViewById(R.id.imgArrow)).setBackgroundResource(R.drawable.arrow_down);
        }

        int drawableResourceId = MainActivity.context.getResources().getIdentifier(oneValuteToday.getImageName(), drawStr, MainActivity.context.getPackageName());
        if (drawableResourceId == 0) {
            drawableResourceId = MainActivity.context.getResources().getIdentifier("kotik", drawStr, MainActivity.context.getPackageName());
        }

        ((TextView) view.findViewById(R.id.tvValuteCharCode)).setText(oneValuteToday.getCharCode());
        ((TextView) view.findViewById(R.id.tvValuteName)).setText(oneValuteToday.getName());
        ((TextView) view.findViewById(R.id.tvTodayValutePrice)).setText(priceToday + "");

        ((ImageView) view.findViewById(R.id.ivFlag)).setBackgroundResource(drawableResourceId);

        return view;
    }

}
