//package com.chikieblan4ik.plsxmlplssssssss;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//
//import java.io.ByteArrayInputStream;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//
//import com.chikieblan4ik.plsxmlplssssssss.Api;
//
//public class ValuteLogic {
////
////    private String xmlUrlString = "http://www.cbr.ru/scripts/XML_daily.asp";
////
////    public void mama44009() {
////        Calendar calDateToday = Calendar.getInstance();
////        Calendar calDateYesterday = Calendar.getInstance();
////        calDateYesterday.add(Calendar.DATE, -1);
////
////        String xmlStringToday = new Api().get(makeUrlWithDate(calDateToday));
////        String xmlStringYesterday = new Api().get(makeUrlWithDate(calDateYesterday));
////
////        АdapterValute adapter = new AdapterValute(this, dictionary, dictionary);
////        listValutes.setAdapter(adapter);
////    }
////
////    public String makeUrlWithDate(Calendar someDate) {
////        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
////
////        StringBuilder xmlUrl = new StringBuilder();
////        xmlUrl.append(xmlUrlString);
////        xmlUrl.append("?date_req=");
////        xmlUrl.append(simpleDateFormat.format(someDate.getTime()));
////        return  xmlUrl.toString();
////    }
////
//
//    public ArrayList<Valute> createValuteArrayToday(String xmlString) {
//        try {
//            ArrayList<Valute> valutesArrayList = new ArrayList<>();
//            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder builder = factory.newDocumentBuilder();
//            Document document = builder.parse(new ByteArrayInputStream(xmlString.getBytes("windows-1251")));
//
//            NodeList valuteNodes = document.getElementsByTagName("Valute");
//
//            for (int i = 0; i < valuteNodes.getLength(); i++) {
//                Node valuteNode = valuteNodes.item(i);
//                Element valuteElement = (Element) valuteNode;
//                String numCode = getValueFromElementByTagName("NumCode", valuteElement);
//                String charCode = getValueFromElementByTagName("CharCode", valuteElement);
//                String nominal = getValueFromElementByTagName("Nominal", valuteElement);
//                String name = getValueFromElementByTagName("Name", valuteElement);
//                String price = getValueFromElementByTagName("Value", valuteElement);
//                Valute valute = new Valute(numCode, charCode, nominal, name, price);
//
//                valutesArrayList.add(valute);
//            }
//            return valutesArrayList;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public Document getDocument(String xmlString) {
//        try {
//            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder builder = factory.newDocumentBuilder();
//            Document document = builder.parse(new ByteArrayInputStream(xmlString.getBytes("windows-1251")));
//            return document;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    public Map<String, Valute> createValuteArrayYesterday(Document document) {
//        try {
//            Map<String, Valute> valutesMap = new HashMap<>();
//            NodeList valuteNodes = document.getElementsByTagName("Valute");
//
//            for (int i = 0; i < valuteNodes.getLength(); i++) {
//                Node valuteNode = valuteNodes.item(i);
//                Element valuteElement = (Element) valuteNode;
//                String numCode = getValueFromElementByTagName("NumCode", valuteElement);
//                String charCode = getValueFromElementByTagName("CharCode", valuteElement);
//                String nominal = getValueFromElementByTagName("Nominal", valuteElement);
//                String name = getValueFromElementByTagName("Name", valuteElement);
//                String price = getValueFromElementByTagName("Value", valuteElement);
//                Valute valute = new Valute(numCode, charCode, nominal, name, price);
//
//                valutesMap.put(charCode, valute);
//            }
//            return valutesMap;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
////
////    public Map<String, Valute> createValutesMap(ArrayList<Valute> valuteArray) {
////        Map<String, Valute> valutesMap = new HashMap<>();
////        for (int i = 0; i < valuteArray.size(); i++) {
////            valutesMap.put(valuteArray.get(i).getCharCode(), valuteArray.get(i));
////        }
////        return valutesMap;
////    }
////
//    public String getValueFromElementByTagName(String tagName, Element element) {
//        return element.getElementsByTagName(tagName).item(0).getTextContent();
//    }
//}
