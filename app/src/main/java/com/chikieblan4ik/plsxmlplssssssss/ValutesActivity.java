package com.chikieblan4ik.plsxmlplssssssss;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class ValutesActivity extends AppCompatActivity {

    private Button btn;
    public static final String TAG = "chikibombom";
    private ListView listValutes;
    private TextView tvDate;
    private String strValute = "Valute";
    private String xmlUrlString = "http://www.cbr.ru/scripts/XML_daily.asp";

    @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valutes);
        getSupportActionBar().hide();
        listValutes = findViewById(R.id.listValutes);
        tvDate = findViewById(R.id.tvDate);

        Calendar calDateToday = Calendar.getInstance();
        Calendar calDateYesterday = Calendar.getInstance();
        calDateYesterday.add(Calendar.DATE, -1);
        tvDate.setText(simpleDateFormat.format(calDateToday.getTime()));

        String xmlStringToday = new Api().get(makeUrlWithDate(calDateToday));
        String xmlStringYesterday = new Api().get(makeUrlWithDate(calDateYesterday));
        Log.i(TAG, makeUrlWithDate(calDateToday));
        Log.i(TAG, makeUrlWithDate(calDateYesterday));

        ArrayList<Valute> valutesArrToday = createValuteArrayToday(xmlStringToday);
        Map<String, Valute> valutesMapYesterday = createValuteArrayYesterday(xmlStringYesterday);

        AdapterValute adapter = new AdapterValute(this, valutesArrToday, valutesMapYesterday);
        listValutes.setAdapter(adapter);
    }

    public String makeUrlWithDate(Calendar someDate) {
        StringBuilder xmlUrl = new StringBuilder();
        xmlUrl.append(xmlUrlString);
        xmlUrl.append("?date_req=");
        xmlUrl.append(simpleDateFormat.format(someDate.getTime()));
        return  xmlUrl.toString();
    }

    Document document;
    public Document getDocument(final String xmlString) {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    document = builder.parse(new ByteArrayInputStream(xmlString.getBytes("windows-1251")));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "run: " + e);
                }
            }
        };
        Thread th = new Thread(run);
        try {
            th.start();
            th.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "getDocument: " + e);
        }
        return document;
    }

    public Valute getOneValute(Element valuteElement) {
        try {
            String numCode = getValueFromElementByTagName("NumCode", valuteElement);
            String charCode = getValueFromElementByTagName("CharCode", valuteElement);
            String nominal = getValueFromElementByTagName("Nominal", valuteElement);
            String name = getValueFromElementByTagName("Name", valuteElement);
            double price = Double.parseDouble(getValueFromElementByTagName("Value", valuteElement));
            Valute valute = new Valute(numCode, charCode, nominal, name, price);
            return valute;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "getOneValute: " + e);
            return null;
        }
    }

    public String getOneValuteCharCode(Element valuteElement) { // только так вышло
        try {
            String numCode = getValueFromElementByTagName("NumCode", valuteElement);
            String charCode = getValueFromElementByTagName("CharCode", valuteElement);
            String nominal = getValueFromElementByTagName("Nominal", valuteElement);
            String name = getValueFromElementByTagName("Name", valuteElement);
            double price = Double.parseDouble(getValueFromElementByTagName("Value", valuteElement));
            Valute valute = new Valute(numCode, charCode, nominal, name, price);
            return valute.getCharCode();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "getOneValute: " + e);
            return null;
        }
    }

    public String getValueFromElementByTagName(String tagName, Element element) {
        return element.getElementsByTagName(tagName).item(0).getTextContent().replaceAll(",", ".");
    }

    public ArrayList<Valute> createValuteArrayToday(String xmlUrlString) {
        try {
            ArrayList<Valute> valutesArrayList = new ArrayList<>();
            NodeList valuteNodes = getDocument(xmlUrlString).getElementsByTagName(strValute);

            for (int i = 0; i < valuteNodes.getLength(); i++) {
                Node valuteNode = valuteNodes.item(i);
                Element valuteElement = (Element) valuteNode;
                valutesArrayList.add(getOneValute(valuteElement));
            }
            return valutesArrayList;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createValuteArrayToday: " + e);
            return null;
        }
    }

    public Map<String, Valute> createValuteArrayYesterday(String xmlUrlString) {
        Map<String, Valute> valutesMap = new HashMap<>();
        try {
            NodeList valuteNodes = getDocument(xmlUrlString).getElementsByTagName(strValute);

            for (int i = 0; i < valuteNodes.getLength(); i++) {
                Node valuteNode = valuteNodes.item(i);
                Element valuteElement = (Element) valuteNode;
                String temp = getOneValuteCharCode(valuteElement);
                valutesMap.put(temp, getOneValute(valuteElement));
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createValuteArrayYesterday: " + e);
        }
        return valutesMap;
    }

//    private void closeActivity() {
//        this.finish();
//    }
}